import {BrowserRouter as Router, Route, Redirect} from "react-router-dom";

import Carousel3d from './NeedToDe/Carousel3d/Index'
import Home from './NeedToDe/Home/Index'

export default function App() {
    return (
        <Router>
            <div>
                <Route exact path='/' render={() => <Redirect to='/home'/>}/>
                <Route path='/home' component={Home}/>
                <Route path='/xuanzhuan' component={Carousel3d}/>
            </div>

        </Router>
    )
}
